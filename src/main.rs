extern crate clap;
extern crate cardio;

use cardio::cardio::Exercise;
use clap::{App, Arg};

fn main() {
    let matches = App::new("Cardio exercise")
    .version("0.1.2")
    .author("Agent Alfa")
    .about("Gives an audio clue for beginning and ending of exercises")
    .arg(
        Arg::with_name("exercises")
        .short("e")
        .takes_value(true)
        .required(true)
        .help("A set of exercises split by comma: Example: 'burpees, jumping jacks'")

    )
    .arg(
        Arg::with_name("rounds")
        .short("r")
        .takes_value(true)
        .required(false)
        .default_value("3")
        .help("Number of rounds for each exercise")
    )
    .arg(
        Arg::with_name("exercise_length")
        .short("l")
        .takes_value(true)
        .required(false)
        .default_value("30")
        .help("Length of each exercise in seconds")
    )
    .arg(
        Arg::with_name("break_length")
        .short("b")
        .takes_value(true)
        .required(false)
        .default_value("5")
        .help("Length of break between exercises in seconds")
    )
    .get_matches();

    let exercises = matches.value_of("exercises").expect("Could not parse the exercises");
    let rounds: u64 = matches.value_of("rounds").expect("Could not parse the number of rounds").trim().parse().expect("Could not parse the rounds value");
    let exercise_length: u64 = matches.value_of("exercise_length").expect("Could not parse the exercise length").trim().parse().expect("Could not parse the length of the exercise");
    let break_length: u64 = matches.value_of("break_length").expect("Could not parse length of the break between exercises").trim().parse().expect("Could not parse the break of the length");


    let exercise = Exercise::new(exercises, rounds, exercise_length, break_length);
    exercise.start();
}
